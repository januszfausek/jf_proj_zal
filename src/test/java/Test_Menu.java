import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_Menu extends Selenium_Base_Steps {

    @Test
    public void menuWorkingTest() {

        new LoginPage(driver)
                .correctLogin()
                .submitLogin()
                .goToProcesses()
                        .assertProcessesUrl()
                        .assertProcessesHeader()
                        .goToCharacteristic()
                                .assertCharacteristicsUrl()
                                .assertCharacteristicsHeader()
                                .goToDashboard()
                                        .assertDashUrl()
                                        .assertDashHeader();
    }
}
