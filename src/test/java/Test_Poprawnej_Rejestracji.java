import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.RegisterPage;

public class Test_Poprawnej_Rejestracji extends Selenium_Base_Steps {

    @Test
    public void correctRegistrationTest() {

        LoginPage loginPage = new LoginPage(driver);

        RegisterPage registerpage = loginPage.submitRegister();
        registerpage.typeRandomEmail();
        String email = registerpage.getEmail();
        registerpage.typePassword("^Password1");
        registerpage.confirmPassword("^Password1");

        HomePage homePage = registerpage.submitRegister();
        homePage.assertWelcomeElementIsShown(email);
    }
}
