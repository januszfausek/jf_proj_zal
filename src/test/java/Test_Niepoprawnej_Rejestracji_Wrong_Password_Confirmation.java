import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;

public class Test_Niepoprawnej_Rejestracji_Wrong_Password_Confirmation extends Selenium_Base_Steps{

    @Test
    public void incorrectPasswordConfirmationTest() {

        LoginPage loginPage = new LoginPage(driver);

        RegisterPage registerpage = loginPage.submitRegister();
        registerpage.typeRandomEmail();
        registerpage.typePassword("^Password1");
        registerpage.confirmPassword("^Password11111");
        registerpage.submitRegister();
        registerpage.assertConfirmationPasswordErrorWarningIsShown();
    }
}
