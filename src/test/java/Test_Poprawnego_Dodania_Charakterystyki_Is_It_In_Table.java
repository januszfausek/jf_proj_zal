import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_Poprawnego_Dodania_Charakterystyki_Is_It_In_Table  extends Selenium_Base_Steps{

    String processName = "DEMO PROJECT";
    String characteristicName = UUID.randomUUID().toString().substring(0, 10);
    String lsl = "8";
    String usl = "10";
    String histo = "0";

    @Test
    public void addCharacteristicToTable (){

        new LoginPage(driver)
                .correctLogin()
                .submitLogin()
                        .goToCharacteristic()
                            .clickonaddNewCharacteristic()
                            .createCharacteristic("DEMO PROJECT", characteristicName, "8", "10", "0")
                            .submitCharacteristic()
                                .assertCharacteristic(characteristicName, lsl, usl, histo);
    }
}
