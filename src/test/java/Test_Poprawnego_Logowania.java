import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_Poprawnego_Logowania extends Selenium_Base_Steps{
    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .correctLogin()
                .submitLogin()
                .assertWelcomeElementIsShown("test@test.com");
    }
}
