import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_Niepoprawnej_Rejestracji_Wrong_Password extends Selenium_Base_Steps{

    @DataProvider
    public Object[][] WrongPasswordsProvider() {
        return new Object[][]{
                {"^nocapital555", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"^noDecimal", "Passwords must have at least one digit ('0'-'9')."},
                {"noSpecial555", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "WrongPasswordsProvider")
    public void incorrectPasswordTest(String wrongPassword, String expectedErrorMessage) {

        new LoginPage(driver)
                .submitRegister()
                .typeRandomEmail()
                .typePassword(wrongPassword)
                .confirmPassword(wrongPassword)
                .failedsubmitRegister()
                .assertRegisterErrorIsShown(expectedErrorMessage);
    }
}
