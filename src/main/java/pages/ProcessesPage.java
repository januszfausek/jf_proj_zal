package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class ProcessesPage extends HomePage {

    String procPageUrl = "http://localhost:4444/Projects";

    @FindBy(css = ".page-title .title_left >h3")
    private WebElement processesElm;

    public ProcessesPage (WebDriver driver) {
        super (driver);
    }

    public ProcessesPage assertProcessesUrl () {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Assert.assertEquals(driver.getCurrentUrl(), procPageUrl, "Wrong url");
        return this;
    }

    public ProcessesPage assertProcessesHeader () {
        Assert.assertEquals(processesElm.getText(), "Processes", "Wrong header");
        return this;
    }
}
