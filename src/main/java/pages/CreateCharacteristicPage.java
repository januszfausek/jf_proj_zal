package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CreateCharacteristicPage {

    protected WebDriver driver;

    public CreateCharacteristicPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(id = "ProjectId")
    private WebElement selectProject;
    @FindBy(id = "Name")
    private WebElement nameTxt;
    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecLimitTxT;
    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecLimitTxT;
    @FindBy(id = "HistogramBinCount")
    private WebElement histoBinCountTxT;
    @FindBy(css= "input[type=submit]")
    private WebElement createCharbtn;

    public CreateCharacteristicPage selectProcess(String processName) {
        new Select(selectProject).selectByVisibleText(processName);
        return this;
    }

    public CreateCharacteristicPage typeName(String name) {
        nameTxt.clear();
        nameTxt.sendKeys(name);
        return this;
    }

    public CreateCharacteristicPage typeLsl(String lsl) {
        lowerSpecLimitTxT.clear();
        lowerSpecLimitTxT.sendKeys(lsl);
        return this;
    }

    public CreateCharacteristicPage typeUsl(String usl) {
        upperSpecLimitTxT.clear();
        upperSpecLimitTxT.sendKeys(usl);
        return this;
    }

    public CreateCharacteristicPage typeHistogramBinCount(String histo) {
        histoBinCountTxT.clear();
        histoBinCountTxT.sendKeys(histo);
        return this;
    }

    public CreateCharacteristicPage createCharacteristic (String projectName, String charactName, String Lsl, String Usl, String histoValue) {
        selectProcess(projectName);
        typeName(charactName);
        typeLsl(Lsl);
        typeUsl(Usl);
        typeHistogramBinCount(histoValue);
        return this;
    }

    public CharacteristicsPage submitCharacteristic() {
        createCharbtn.click();
        return new CharacteristicsPage(driver);
    }

}
