package pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

import static java.lang.System.currentTimeMillis;

public class RegisterPage {

    protected WebDriver driver;

    public RegisterPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(id="Email")
    private WebElement emailTxt;
    @FindBy(id="Password")
    private WebElement passwordTxt;
    @FindBy(id="ConfirmPassword")
    private WebElement confirmpasswordTxt;
    @FindBy(css="button[type=submit]")
    private WebElement registerBtn;
    @FindBy(id="ConfirmPassword-error")
    public WebElement confirmpassworderrorTxt;
    @FindBy(css=".validation-summary-errors li")
    public List<WebElement> registerErrors;


    public RegisterPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public RegisterPage typeRandomEmail() {
        String email = RandomStringUtils.randomAlphabetic(1) + RandomStringUtils.randomAlphanumeric(5) + currentTimeMillis();
        email = email + "@" + RandomStringUtils.randomAlphabetic(5).toLowerCase() + "." + RandomStringUtils.randomAlphabetic(3).toLowerCase();
        emailTxt.sendKeys(email);
        System.out.println(email);
        return this;
    }

    public String getEmail () {
        return emailTxt.getText();
    }

    public RegisterPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public RegisterPage confirmPassword(String confirmpassword) {
        confirmpasswordTxt.clear();
        confirmpasswordTxt.sendKeys(confirmpassword);
        return this;
    }

    public HomePage submitRegister() {
        registerBtn.click();
        return new HomePage(driver);
    }

    public RegisterPage failedsubmitRegister() {
        registerBtn.click();
        return this;
    }

    public RegisterPage assertConfirmationPasswordErrorWarningIsShown() {
        Assert.assertTrue(confirmpassworderrorTxt.isDisplayed(), "Warning, there is no info about wrong password confirmation displayed.");
        Assert.assertTrue(confirmpassworderrorTxt.getText().contains("The password and confirmation password do not match."), "Wrong info, should be: \"The password and confirmation password do not match.\"");
        return this;
    }

    public RegisterPage assertRegisterErrorIsShown(String expectedError){
        boolean areErrorsDisplayed = registerErrors.stream().allMatch(error -> error.isDisplayed());
        boolean isErrorShown = registerErrors.stream().anyMatch(error -> error.getText().equals(expectedError));
        Assert.assertTrue(areErrorsDisplayed, "Errors are not displayed");
        Assert.assertTrue(isErrorShown, "Error: '" + expectedError + "' is not shown");
        return this;
    }

}
