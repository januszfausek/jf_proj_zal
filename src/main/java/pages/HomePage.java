package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {
    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css=".menu-home")
    private WebElement homeNav;
    @FindBy(css=".nav.child_menu>li>a")
    private WebElement dashboardMenu;

    @FindBy(css=".menu-workspace")
    private WebElement workspaceNav;
    @FindBy(css="a[href$=Projects]")
    private WebElement processesMenu;
    @FindBy(css="a[href$=Characteristics]")
    private WebElement charactericticsMenu;

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));
        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses() {
        if (!isParentExpanded(workspaceNav)) workspaceNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));
        processesMenu.click();
        return new ProcessesPage(driver);
    }

    public CharacteristicsPage goToCharacteristic() {
        if (!isParentExpanded(workspaceNav)) workspaceNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(charactericticsMenu));
        charactericticsMenu.click();
        return new CharacteristicsPage(driver);
    }

    public DashboardPage goToDashboard() {
        if (!isParentExpanded(homeNav)) homeNav.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));
        dashboardMenu.click();
        return new DashboardPage(driver);
    }

    public HomePage assertWelcomeElementIsShown(String email) {
        Assert.assertTrue(welcomeElm.isDisplayed(), "Welcome element is not shown." );
        Assert.assertTrue(welcomeElm.getText().contains(email), "Welcome element text: '" + welcomeElm.getText() + " does not contain " + email);
        return this;
    }
}
