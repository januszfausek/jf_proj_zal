package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class DashboardPage extends HomePage{

    String dashboardPageUrl ="http://localhost:4444/";

    @FindBy(css = ".x_title > h2")
    private WebElement dashElm;

    public DashboardPage (WebDriver driver) {
        super (driver);
    }

    public DashboardPage assertDashUrl () {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Assert.assertEquals(driver.getCurrentUrl(), dashboardPageUrl, "Wrong url");
        return this;
    }

    public DashboardPage assertDashHeader () {
        Assert.assertEquals(dashElm.getText(), "DEMO PROJECT", "Wrong header");
        return this;
    }
}
