package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class CharacteristicsPage extends HomePage{

    String charPageUrl = "http://localhost:4444/Characteristics";
    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[text()='%s']/..";

    @FindBy(css = ".page-title .title_left >h3")
    private WebElement charactHeader;
    @FindBy(css="a[href*='/Characteristics/Create']")
    private WebElement addnewcharacteristicBtn;

    public CharacteristicsPage (WebDriver driver) {
        super (driver);
    }

    public CharacteristicsPage assertCharacteristicsUrl () {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Assert.assertEquals(driver.getCurrentUrl(), charPageUrl, "Wrong url");
        return this;
    }

    public CharacteristicsPage assertCharacteristicsHeader () {
        Assert.assertEquals(charactHeader.getText(), "Characteristics", "Wrong header");
        return this;
    }

    public CreateCharacteristicPage clickonaddNewCharacteristic() {
        addnewcharacteristicBtn.click();
        return new CreateCharacteristicPage(driver);
    }


    public CharacteristicsPage assertCharacteristic(String expName, String expLsl, String expUsl, String expHisto) {
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));
        String actLsl = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String actUsl = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String actBinCount = characteristicRow.findElement(By.xpath("./td[5]")).getText();
        Assert.assertEquals(actLsl, expLsl);
        Assert.assertEquals(actUsl, expUsl);
        Assert.assertEquals(actBinCount, expHisto);
        return this;
    }
}
