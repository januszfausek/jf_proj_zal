package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {
    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(id="Email")
    private WebElement emailTxt;
    @FindBy(id="Password")
    private WebElement passwordTxt;
    @FindBy(css="button[type=submit]")
    private WebElement loginBtn;
    @FindBy(css="a[href*='/Account/Register']")
    private WebElement Register_new_userTxt;

    // @FindBy(xpath="//li[contains(text(),'Invalid login attempt.')]")
    @FindBy(css=".validation-summary-errors li")
    public List<WebElement> loginErrors;

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }
    public LoginPage typePassword(String pass) {
        passwordTxt.clear();
        passwordTxt.sendKeys(pass);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public RegisterPage submitRegister() {
        Register_new_userTxt.click();
        return new RegisterPage(driver);
    }

    public LoginPage correctLogin() {
        typeEmail("test@test.com");
        typePassword("Test1!");
        return this;
    }

    public LoginPage assertLoginErrorIsShown(String expError){
        boolean areErrorsDisplayed = loginErrors.stream().allMatch(error -> error.isDisplayed());
        boolean isErrorShown = loginErrors.stream().anyMatch(error -> error.getText().equals(expError));
        Assert.assertTrue(areErrorsDisplayed, "Errors are not displayed");
        Assert.assertTrue(isErrorShown, "Error: '" + expError + "' is not shown");
        return this;
    }
}
